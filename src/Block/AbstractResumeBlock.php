<?php
declare(strict_types=1);
namespace Nakima\AdminBundle\Block;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

abstract class AbstractResumeBlock extends AbstractBlock
{

    public function getType()
    {
        return 'resume';
    }

    public function getTemplate()
    {
        return "NakimaAdminBundle:Block:resume.html.twig";
    }

    abstract function getIconClass();

    abstract function getTitle();

    abstract function getSubtitle();

    abstract function getLinkLabel();

    abstract function getColor();

    abstract function getDestination();
}
