<?php
declare(strict_types=1);
namespace Nakima\AdminBundle\Block;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

class EntityBlock extends AbstractBlock
{

    protected $action;
    protected $bundleName;
    protected $entityName;

    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    public function getTemplate()
    {
        return "NakimaAdminBundle:Core:$this->action.html.twig";
    }

    public function getBundleName()
    {
        return $this->bundleName;
    }

    public function getEntityName()
    {
        return $this->entityName;
    }

    public function getRowTemplate()
    {
        return "NakimaAdminBundle:Block:group_row.html.twig";
    }

    public function setBlockInfo($blockInfo)
    {
        parent::setBlockInfo($blockInfo);

        $clzz = $blockInfo["meta"]["entity"];

        $matches = [];
        preg_match("/\\w+$/", $clzz, $matches);
        $this->entityName = $matches[0];

        $matches = [];
        preg_match("/(\w+)Bundle/", str_replace("\\", '', $clzz), $matches);
        $this->bundleName = $matches[1];

        $this->blockInfo = $blockInfo;

        return $this;
    }
}
