<?php
declare(strict_types=1);
namespace Nakima\AdminBundle\Block;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

abstract class AbstractBlock
{

    protected $inGroup;

    protected function getContainer()
    {
        global $kernel;
        if ($kernel instanceOf \AppCache) {
            $kernel = $kernel->getKernel();
        }

        return $kernel->getContainer();
    }

    public function getDoctrine()
    {
        return $this->getContainer()->get('doctrine');
    }

    public function isInGroup()
    {
        return $this->inGroup;
    }

    public function setInGroup($inGroup)
    {
        $this->inGroup = $inGroup;

        return $this;
    }

    public function getUser()
    {

        if ($this->getContainer()->get('security.token_storage')->getToken()) {
            return $this->getContainer()->get('security.token_storage')->getToken()->getUser();
        }

        return null;
    }

    public function getBlockInfo()
    {
        return $this->blockInfo;
    }

    public function setBlockInfo($blockInfo)
    {
        $this->blockInfo = $blockInfo;

        return $this;
    }

    abstract public function getTemplate();
}
