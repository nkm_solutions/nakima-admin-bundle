<?php
declare(strict_types=1);

namespace Nakima\AdminBundle\Admin;

/**
 * @author xgonzalez@nakima.es
 */

use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Symfony;

abstract class AbstractBaseAdmin
{

    protected $subject;
    protected $action;
    protected $entityFqn;
    protected $request;

    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    public function getSubject(): BaseEntity
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    public function getSettings()
    {
        return [];
    }

    public function getContainer()
    {
        return Symfony::getContainer();
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    public function getRepo($fqn = null)
    {
        if (!$fqn) {
            $fqn = $this->entityFqn;
        }

        return $this->getContainer()->get('doctrine')->getRepository($fqn);
    }

    public function getEntityFqn()
    {
        return $this->entityFqn;
    }

    public function setEntityFqn($entityFqn)
    {
        $this->entityFqn = $entityFqn;

        return $this;
    }

    public function allow($user, $role, $action, $subjet = null)
    {
        return $role == "IS_AUTHENTICATED_ANONYMOUSLY" || ($user && $user->grantsRole([$role]));
    }

    public function filter($query, $e)
    {
    }

    public function getTemplate($key)
    {
        if ($key == "dashboard_group_row") {
            return "NakimaAdminBundle:Block:group_row.html.twig";
        }

        return null;
    }

    abstract public function createFields($form);

    abstract public function listFields($form);

    abstract public function editFields($form);

    abstract public function showFields($form);

    abstract public function preCreate($validator);

    abstract public function preUpdate($validator);

    abstract public function preDelete($validator);

    abstract public function postCreate();

    abstract public function postUpdate();

    abstract public function postDelete();

}

