<?php
declare(strict_types=1);
namespace Nakima\AdminBundle\Admin;

/**
 * @author xgonzalez@nakima.es
 */

class BaseAdmin extends AbstractBaseAdmin
{

    public function createFields($form)
    {
        $reflect = new \ReflectionClass($this->getSubject());
        $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);

        foreach ($props as $prop) {
            $form->add($prop->getName());
        }

        return $form;
    }

    public function listFields($form)
    {
        $reflect = new \ReflectionClass($this->getSubject());
        $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);

        foreach ($props as $prop) {
            $form->add($prop->getName());
        }

        return $form;
    }

    public function editFields($form)
    {
        return $this->createFields($form);
    }

    public function showFields($form)
    {
        $reflect = new \ReflectionClass($this->getSubject());
        $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);

        foreach ($props as $prop) {
            $form->add($prop->getName());
        }

        return $form;
    }

    public function preDelete($validator)
    {
    }

    public function preCreate($validator)
    {
    }

    public function preUpdate($validator)
    {
    }

    public function postDelete()
    {
    }

    public function postCreate()
    {
    }

    public function postUpdate()
    {
    }
}
