<?php
declare(strict_types=1);
namespace Nakima\AdminBundle\Admin;

/**
 * @author xgonzalez@nakima.es
 */

use Sonata\AdminBundle\Admin\AbstractAdmin as SonataAdmin;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class Admin extends SonataAdmin
{

    protected $container;

    public function __construct($code, $class, $baseControllerName, ContainerInterface $container)
    {
        parent::__construct($code, $class, $baseControllerName, $container);
        $this->container = $container;
    }

    public function getUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }

    public function hasRole($role)
    {
        return $this->container->get('security.authorization_checker')->isGranted($role);
    }

    public function checkRole($role)
    {
        trigger_error(
            __METHOD__." is deprecated, it will be removed in the stable version, use ".__CLASS__."::hasRole instead",
            E_USER_DEPRECATED
        );

        return $this->container->get('security.authorization_checker')->isGranted($role);
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function getEntitymanager()
    {
        return $this->container->get('doctrine');
    }

    public function getPersistentParameters()
    {
        $parameters = parent::getPersistentParameters();

        if (!$this->getRequest()) {
            return array();
        }

        return $this->getRequest()->query->all();
    }

    /*
    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);

        if ($this->checkRole("ROLE_ADMIN")) {
            return $query;
        }


        $user = $this->getuser();

        $class = $this->getClass();

        $object = new $class();

        if ($object instanceof Ownable) {
            $field = $object->getOwnerField();

            $query->andWhere(
                $query->expr()->eq($query->getRootAliases()[0] . ".association" , ':user')
            );
            $query->setParameter('user', $user->getID());
        }
        return $query;
    }
    */
}
