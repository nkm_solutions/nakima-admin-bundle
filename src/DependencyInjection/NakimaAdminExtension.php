<?php
declare(strict_types=1);
namespace Nakima\AdminBundle\DependencyInjection;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class NakimaAdminExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('nakima_admin', $config);
    }
}
