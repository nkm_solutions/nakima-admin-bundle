<?php
declare(strict_types=1);
namespace Nakima\AdminBundle\DependencyInjection;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder;
        $rootNode = $treeBuilder->root('nakima_admin');

        $rootNode
            ->children()
                ->scalarNode('theme')->defaultValue('blue')->end()
                ->scalarNode('mode')->defaultValue('fixed')->end()
                ->arrayNode('dashboard')
                    ->children()
                        ->arrayNode('blocks')
                            ->prototype('array')
                                ->children()
                                    ->scalarNode('block')->end()
                                    ->scalarNode('group')->end()
                                    ->scalarNode('menu')->end()
                                    ->scalarNode('role')->defaultValue("IS_AUTHENTICATED_ANONYMOUSLY")->end()
                                    ->enumNode('position')->values(['menu', 'top', 'bottom', 'fourth', 'third', 'half', 'full'])->defaultValue('full')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('menus')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('label')->isRequired()->end()
                            ->scalarNode('route')->isRequired()->end()
                            ->scalarNode('translator')->defaultValue('messages')->end()
                            ->scalarNode('icon')->defaultValue('')->end()
                            ->scalarNode('icon_color')->defaultValue('')->end()
                            ->arrayNode('meta')
                                ->useAttributeAsKey('name')
                                    ->prototype('scalar')
                                ->end()
                            ->end()
                            ->scalarNode('role')->defaultValue("IS_AUTHENTICATED_ANONYMOUSLY")->end()
                            ->arrayNode('groups')->prototype('scalar')->end()->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('blocks')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('class')->isRequired()->end()
                            ->scalarNode('icon')->defaultValue('')->end()
                            ->scalarNode('translator')->defaultValue('messages')->end()
                            ->scalarNode('icon_color')->defaultValue('')->end()
                            ->arrayNode('meta')
                                ->useAttributeAsKey('name')
                                    ->prototype('scalar')
                                ->end()
                            ->end()
                            ->scalarNode('role')->defaultValue("IS_AUTHENTICATED_ANONYMOUSLY")->end()
                            ->arrayNode('groups')->prototype('scalar')->end()->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('groups')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('label')->end()
                            ->scalarNode('translator')->defaultValue('messages')->end()
                            ->scalarNode('role')->defaultValue("IS_AUTHENTICATED_ANONYMOUSLY")->end()
                            ->scalarNode('icon')->defaultValue("circle-o")->end()
                            ->scalarNode('icon_color')->defaultValue("white")->end()
                            ->arrayNode('blocks')
                                ->prototype('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('templates')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('base')->defaultValue('NakimaAdminBundle::base.html.twig')->end()
                        ->scalarNode('dashboard')->defaultValue('NakimaAdminBundle:Core:dashboard.html.twig')->end()
                        ->scalarNode('create')->defaultValue('NakimaAdminBundle:Core:create.html.twig')->end()
                        ->scalarNode('edit')->defaultValue('NakimaAdminBundle:Core:edit.html.twig')->end()
                        ->scalarNode('show')->defaultValue('NakimaAdminBundle:Core:show.html.twig')->end()
                        ->scalarNode('list')->defaultValue('NakimaAdminBundle:Core:list.html.twig')->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }

}
/*
->arrayNode('templates')
    ->addDefaultsIfNotSet()
    ->children()
        ->scalarNode('login')->defaultValue('NakimaUserBundle:User:login.html.twig')->end()
        ->scalarNode('register')->defaultValue('NakimaUserBundle:User:register.html.twig')->end()
        ->scalarNode('register_complete')->defaultValue('NakimaUserBundle:User:register_complete.html.twig')->end()
        ->scalarNode('register_validate')->defaultValue('NakimaUserBundle:User:register_validate.html.twig')->end()
        ->scalarNode('password_recover')->defaultValue('NakimaUserBundle:User:password_recover.html.twig')->end()
        ->scalarNode('password_recover_complete')->defaultValue('NakimaUserBundle:User:password_recover_complete.html.twig')->end()
        ->scalarNode('password_new')->defaultValue('NakimaUserBundle:User:password_new.html.twig')->end()
    ->end()
->end()
*/
