<?php
declare(strict_types=1);

namespace Nakima\AdminBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Model\DataMapper;
use Symfony\Component\HttpFoundation\Request;

class BaseAdminController extends BaseController
{

    protected $configBlock;
    protected $block;
    protected $admin;
    protected $entityFqn;
    protected $controllerFqn;

    public function listAction(Request $request, $entity = null)
    {
        $bundle = ucfirst($request->attributes->get('bundle'));
        $entity = ucfirst($request->attributes->get('entity'));

        $this->configure($bundle, $entity);
        $this->admin->setRequest($request);
        $this->admin->setAction('list');
        $this->block->setAction('list');
        $this->admin->setEntityFqn($this->entityFqn);

        $entityClass = $bundle . "Bundle\\Entity\\$entity";

        $rClass = new \ReflectionClass($entityClass);
        if (!$rClass->isInstantiable()) {
            throw $this->createNotFoundException("The page does not exist");
        }

        $subject = new $entityClass;

        $this->admin->setSubject($subject);

        $query = $this->getRepo($this->entityFqn)->createQueryBuilder('e');

        $this->admin->filter($query, 'e');

        $data = $query->getQuery()->getResult();

        $form = new DataMapper($subject);
        $this->admin->listFields($form);

        return $this->render(
            $this->block->getTemplate(),
            [
                //'form' => $form->createView(),
                'mapper'         => $form,
                'config_block'   => $this->configBlock,
                'block'          => $this->block,
                'admin'          => $this->admin,
                'bundle_name'    => $bundle,
                'entity_name'    => $entity,
                'entity_fqn'     => $this->entityFqn,
                'controller_fqn' => $this->controllerFqn,
                'data'           => $data,
                'settings'       => $this->admin->getSettings(),
                'request'        => $request,
                'translator'     => $this->configBlock["translator"],
            ]
        );
    }

    public function createAction(Request $request, $entity = null)
    {

        $bundle = ucfirst($request->attributes->get('bundle'));
        $entity = ucfirst($request->attributes->get('entity'));

        $this->configure($bundle, $entity);

        $entityClass = $bundle . "Bundle\\Entity\\$entity";
        $subject     = new $entityClass;

        $this->admin->setRequest($request);
        $this->admin->setSubject($subject);
        $this->admin->setAction('create');
        $this->admin->setEntityFqn($this->entityFqn);

        $form = $this->createFormBuilder($subject, ['translation_domain' => $this->configBlock["translator"]]);
        $this->admin->createFields($form);
        $form = $form->getForm();

        $this->block->setAction('create');

        if ($this->isPost()) {
            $form->handleRequest($request);

            $data = $form->getData();
            $this->admin->setSubject($data);
            $this->admin->preCreate(null);

            if (true) {



                $manager = $this->getDoctrine()->getManager();
                $manager->persist($data);
                $manager->flush();

                $this->addFlash(
                    'success',
                    $this->get('translator')->trans('entity_registered_success', [], 'nakima_admin')
                );

                if ($this->optParam("edit", false)) {
                    return $this->redirect(
                        $this->generateUrl(
                            'nakima_admin_id_action',
                            [
                                'bundle' => $bundle,
                                'entity' => $entity,
                                'action' => 'edit',
                                'id'     => $data->getId(),
                            ]
                        ),
                        301
                    );
                }

                return $this->redirect(
                    $this->generateUrl(
                        'nakima_admin_action',
                        [
                            'bundle' => $bundle,
                            'entity' => $entity,
                            'action' => 'list',
                        ]
                    ),
                    301
                );
            } else {
            }
        }

        return $this->render(
            $this->block->getTemplate(),
            [
                'form'           => $form->createView(),
                'config_block'   => $this->configBlock,
                'block'          => $this->block,
                'admin'          => $this->admin,
                'bundle_name'    => $bundle,
                'entity_name'    => $entity,
                'entity_fqn'     => $this->entityFqn,
                'controller_fqn' => $this->controllerFqn,
                'settings'       => $this->admin->getSettings(),
                'data'           => $subject,
                'request'        => $request,
                'translator'     => $this->configBlock["translator"],
            ]
        );
    }

    public function checkEntity($entity)
    {
        if (!$entity) {
            if ($this->isEnvProd()) {
                throw $this->createNotFoundException('The page does not exist');
            }
        }
    }

    public function editIdAction(Request $request, $entity = null)
    {

        $bundleName = ucfirst($request->attributes->get('bundle'));
        $entityName = ucfirst($request->attributes->get('entity'));

        if ($this->optParam("delete", false)) {
            return $this->redirect(
                $this->generateUrl(
                    'nakima_admin_id_action',
                    [
                        'bundle' => $bundleName,
                        'entity' => $entityName,
                        'action' => 'delete',
                        'id'     => $entity->getId(),
                    ]
                ),
                301
            );
        }

        $this->configure($bundleName, $entityName);

        $entityClass = $bundleName . "Bundle\\Entity\\$entityName";
        $subject     = $entity;

        $this->admin->setRequest($request);
        $this->admin->setSubject($subject);
        $this->admin->setAction('edit');

        $form = $this->createFormBuilder($subject, ['translation_domain' => $this->configBlock["translator"]]);
        $this->admin->editFields($form);

        $form = $form->getForm();

        $this->block->setAction('edit');

        if ($this->isPost()) {
            $form->handleRequest($request);
            $data = $form->getData();
            $this->admin->setSubject($data);
            $this->admin->preUpdate(null);
            if (true) {



                $manager = $this->getDoctrine()->getManager();
                $manager->persist($data);
                $manager->flush();

                $this->addFlash(
                    'success',
                    $this->get('translator')->trans('entity_updated_success', [], 'nakima_admin')
                );

                if ($this->optParam("edit", false)) {
                    return $this->redirect(
                        $this->generateUrl(
                            'nakima_admin_id_action',
                            [
                                'bundle' => $bundleName,
                                'entity' => $entityName,
                                'action' => 'edit',
                                'id'     => $data->getId(),
                            ]
                        ),
                        301
                    );
                }

                return $this->redirect(
                    $this->generateUrl(
                        'nakima_admin_action',
                        [
                            'bundle' => $bundleName,
                            'entity' => $entityName,
                            'action' => 'list',
                        ]
                    ),
                    301
                );
            }
        }

        return $this->render(
            $this->block->getTemplate(),
            [
                'data'           => $subject,
                'form'           => $form->createView(),
                'config_block'   => $this->configBlock,
                'block'          => $this->block,
                'admin'          => $this->admin,
                'bundle_name'    => $bundleName,
                'entity_name'    => $entityName,
                'entity_fqn'     => $this->entityFqn,
                'controller_fqn' => $this->controllerFqn,
                'settings'       => $this->admin->getSettings(),
                'data'           => $subject,
                'request'        => $request,
                'translator'     => $this->configBlock["translator"],
            ]
        );
    }

    public function showIdAction(Request $request, $entity = null)
    {

        $bundleName = ucfirst($request->attributes->get('bundle'));
        $entityName = ucfirst($request->attributes->get('entity'));

        $this->configure($bundleName, $entityName);

        $entityClass = $bundleName . "Bundle\\Entity\\$entityName";
        $subject     = $entity;

        $this->admin->setRequest($request);
        $this->admin->setAction('show');
        $this->admin->setSubject($subject);

        $form = $this->createFormBuilder($subject, ['translation_domain' => $this->configBlock["translator"]]);
        $this->admin->showFields($form);
        $form = $form->getForm();

        $this->block->setAction('show');

        return $this->render(
            $this->block->getTemplate(),
            [
                'data'           => $subject,
                'form'           => $form->createView(),
                'config_block'   => $this->configBlock,
                'block'          => $this->block,
                'admin'          => $this->admin,
                'bundle_name'    => $bundleName,
                'entity_name'    => $entityName,
                'entity_fqn'     => $this->entityFqn,
                'controller_fqn' => $this->controllerFqn,
                'settings'       => $this->admin->getSettings(),
                'data'           => $subject,
                'request'        => $request,
                'translator'     => $this->configBlock["translator"],
            ]
        );
    }

    public function deleteIdAction(Request $request, $entity = null)
    {
        $bundleName = ucfirst($request->attributes->get('bundle'));
        $entityName = ucfirst($request->attributes->get('entity'));

        $this->configure($bundleName, $entityName);

        $entityClass = $bundleName . "Bundle\\Entity\\$entityName";
        $subject     = $entity;

        $this->admin->setRequest($request);
        $this->admin->setAction('delete');
        $this->admin->setSubject($subject);

        $resp = $this->admin->preDelete(null);

        if ($resp !== false) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($subject);
            $manager->flush();

            $this->addFlash(
                'success',
                $this->get('translator')->trans('entity_deleted_success', [], 'nakima_admin')
            );
        }

        return $this->redirect(
            $this->generateUrl(
                'nakima_admin_action',
                [
                    'bundle' => $bundleName,
                    'entity' => $entityName,
                    'action' => 'list',
                ]
            ),
            301
        );
    }

    /**
     * Obscure stuff
     */
    public function _actionAction(Request $request, $bundle, $entity, $action)
    {

        $request->attributes->set('bundle', $bundle);
        $request->attributes->set('entity', $entity);

        $this->configure($bundle, $entity);

        $this->admin->setRequest($request);
        $this->admin->setAction($action);
        $user = $this->getUser();
        if (!$this->admin->allow($user, $this->configBlock["role"], $action)) {
            $this->addFlash(
                'denied',
                $this->get('translator')->trans('admin_access_denied', [], 'nakima_admin')
            );

            $referer = $request->headers->get('referer');
            if (!$referer) {
                return $this->redirect($this->generateUrl('nakima_admin_dashboard'), 301);
            }

            return $this->redirect($referer);
        }

        $blocks = $this->getParameter('nakima_admin')["dashboard"]["blocks"];

        $response = $this->forward("$this->controllerFqn:$action", ['request' => $request]);
        if ($response->getStatusCode() == 500) {
            if ($this->isEnvProd()) {
                throw $this->createNotFoundException('The page does not exist');
            }
        }

        return $response;
    }

    public function _actionIdAction(Request $request, $bundle, $entity, $id, $action)
    {

        $request->attributes->set('bundle', $bundle);
        $request->attributes->set('entity', $entity);

        $this->configure($bundle, $entity);

        $this->admin->setRequest($request);
        $this->admin->setAction($action);
        $user   = $this->getUser();
        $entity = $this->getRepo($this->entityFqn)->findOneById($id);

        if (!$entity) {
            throw $this->createNotFoundException('The page does not exist');
        }

        if (!$this->admin->allow($user, $this->configBlock["role"], $action, $entity)) {
            $this->addFlash(
                'denied',
                $this->get('translator')->trans('admin_access_denied', [], 'nakima_admin')
            );
            $referer = $request->headers->get('referer');
            if (!$referer) {
                return $this->redirect($this->generateUrl('nakima_admin_dashboard'), 301);
            }

            return $this->redirect($referer);
        }

        $blocks = $this->getParameter('nakima_admin')["dashboard"]["blocks"];

        $this->checkEntity($entity);

        $response = $this->forward("$this->controllerFqn:$action" . "Id", ['request' => $request, 'entity' => $entity]);
        if ($response->getStatusCode() == 500) {
            if ($this->isEnvProd()) {
                throw $this->createNotFoundException('The page does not exist');
            }
        }

        return $response;
    }

    public function _updateAccountAction(Request $request)
    {
        $entity = $this->getUser();

        $matches = [];
        preg_match("/\\w+$/", get_class($entity), $matches);
        $entityName = $matches[0];

        $matches = [];
        preg_match("/(\w+)Bundle/", str_replace("\\", '', get_class($entity)), $matches);
        $bundleName = $matches[1];

        $this->configure($bundleName, $entityName);

        $entityClass = $bundleName . "Bundle\\Entity\\$entityName";
        $subject     = $entity;

        $this->admin->setRequest($request);
        $this->admin->setSubject($subject);
        $this->admin->setAction('edit');

        $form = $this->createFormBuilder($subject, ['translation_domain' => $this->configBlock["translator"]]);
        $this->admin->editFields($form);

        $form = $form->getForm();

        $this->block->setAction('edit');

        if ($this->isPost()) {
            $form->handleRequest($request);

            if (true) {

                $data = $form->getData();

                $manager = $this->getDoctrine()->getManager();
                $manager->persist($data);
                $manager->flush();

                $this->addFlash(
                    'success',
                    $translated = $this->get('translator')->trans('entity_updated_success', [], 'nakima_admin')
                );

                return $this->redirect(
                    $this->generateUrl(
                        'nakima_admin_action',
                        [
                            'bundle' => $bundleName,
                            'entity' => $entityName,
                            'action' => 'list',
                        ]
                    ),
                    301
                );
            }
        }

        return $this->render(
            $this->block->getTemplate(),
            [
                'data'           => $subject,
                'form'           => $form->createView(),
                'config_block'   => $this->configBlock,
                'block'          => $this->block,
                'admin'          => $this->admin,
                'bundle_name'    => $bundleName,
                'entity_name'    => $entityName,
                'entity_fqn'     => $this->entityFqn,
                'controller_fqn' => $this->controllerFqn,
                'settings'       => $this->admin->getSettings(),
                'data'           => $subject,
                'request'        => $request,
                'translator'     => $this->configBlock["translator"],
            ]
        );
    }

    protected function configure($bundle, $entity)
    {
        //$blocks = $this->getParameter('nakima_admin')["dashboard"]["blocks"];
        $blocks = $this->getParameter('nakima_admin')["blocks"];

        $bundle = ucfirst($bundle);
        $entity = ucfirst($entity);

        $class           = $bundle . "Bundle\\Entity\\$entity";
        $this->entityFqn = $bundle . "Bundle:$entity";

        foreach ($blocks as $key => $block) {
            if (isset($block["meta"]) && isset($block["meta"]["type"]) && $block["meta"]["type"] == "entity") {
                if ($block["meta"]["entity"] == $class) {
                    $this->configBlock = $block;
                    break;
                }
            }
        }

        if (!$this->configBlock) {
            if ($this->isEnvProd()) {
                throw $this->createNotFoundException("The page does not exist");
            }
        }

        $a = $block["meta"]["admin"] ?? 'Nakima\AdminBundle\Admin\BaseAdmin';

        $this->admin         = new $a();
        $this->controllerFqn = $block["meta"]["controller"] ?? 'NakimaAdminBundle:BaseAdmin';

        $this->block = new $block["class"];
    }

    public function _dashboardAction(Request $request)
    {
        return $this->render("NakimaAdminBundle:Core:dashboard.html.twig");
    }
}
