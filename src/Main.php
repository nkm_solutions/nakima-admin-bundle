<?php
declare(strict_types=1);
namespace Base;

class Main
{

    public function add($a, $b)
    {
        return $a + $b;
    }
}
