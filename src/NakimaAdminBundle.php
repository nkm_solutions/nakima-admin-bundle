<?php
declare(strict_types=1);
namespace Nakima\AdminBundle;

/**
 * @author xgonzalez@nakima.es
 */

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaAdminBundle extends Bundle
{
}
